# Game-API-Tiled-Layer-on-Java
mage backgroundImage = Image.createImage("/background_tiles.png");
TiledLayer background = new TiledLayer(8,4 backgroundImage, 48 ,48);
background.setPosition(12, 0);
int[] map = {
	1, 2, 0, 0, 0, 0, 0, 0,
	3, 3, 2, 0, 0, 0, 5, 0,
	3, 3, 3, 2, 4, 1, 3, 2,
	6, 6, 6, 6, 6, 6, 6, 6
};
for (int i = 0 i < map.length;  i++){
	int column = i  % 8;
	int row = (i - column)  / 8;
	background.setCell(column, row, map[i]);
}
